from flask import Flask, render_template, request, redirect
import psycopg2

app = Flask(__name__)

# Conexión a la base de datos
conn = psycopg2.connect(
    dbname="ProyectoFinal",
    user="postgres",
    password="M3y",
    host="localhost"
)
cursor = conn.cursor()

# FUNCIONES

def listadoProfesor():
    sql = "SELECT * FROM profesor"
    cursor.execute(sql)
    result = cursor.fetchall()
    return result

def buscarProfesor(profesor_id):
    sql = "SELECT * FROM profesor WHERE profesor_id=%s"
    cursor.execute(sql, (profesor_id,))
    result = cursor.fetchone()
    return result

def agregarProfesor(nombre_profesor, apellido_profesor, telefono_profesor, correo):
    sql = "INSERT INTO profesor (nombre_profesor, apellido_profesor, telefono_profesor, correo) VALUES (%s, %s, %s, %s)"
    cursor.execute(sql, (nombre_profesor, apellido_profesor, telefono_profesor, correo))
    conn.commit()
    return "Profesor agregado"

def editarProfesor(profesor_id, nombre_profesor, apellido_profesor, telefono_profesor, correo):
    sql = "UPDATE profesor SET nombre_profesor=%s, apellido_profesor=%s, telefono_profesor=%s, correo=%s WHERE profesor_id=%s"
    cursor.execute(sql, (nombre_profesor, apellido_profesor, telefono_profesor, correo, profesor_id))
    conn.commit()
    return "Profesor actualizado"

def eliminarProfesor(profesor_id):
    sql = "DELETE FROM profesor WHERE profesor_id=%s"
    cursor.execute(sql, (profesor_id,))
    conn.commit()
    return "Profesor eliminado"

# RUTAS 


@app.route('/')
def index():
    profesores = listadoProfesor()
    return render_template('gestion_profesores.html', profesores=profesores)

@app.route('/agregar', methods=['GET', 'POST'])
def agregar():
    if request.method == 'POST':
        nombre_profesor = request.form['nombre_profesor']
        apellido_profesor = request.form['apellido_profesor']
        telefono_profesor = request.form['telefono_profesor']
        correo = request.form['correo']
        agregarProfesor(nombre_profesor, apellido_profesor, telefono_profesor, correo)
        return redirect('/')
    return render_template('gestion_profesores.html', profesor=None, profesores=listadoProfesor())

@app.route('/editar/<int:profesor_id>', methods=['GET', 'POST'])
def editar(profesor_id):
    profesor = buscarProfesor(profesor_id)
    if request.method == 'POST':
        nombre_profesor = request.form['nombre_profesor']
        apellido_profesor = request.form['apellido_profesor']
        telefono_profesor = request.form['telefono_profesor']
        correo = request.form['correo']
        editarProfesor(profesor_id, nombre_profesor, apellido_profesor, telefono_profesor, correo)
        return redirect('/')
    return render_template('gestion_profesores.html', profesor=profesor, profesores=listadoProfesor())

@app.route('/eliminar/<int:profesor_id>')
def eliminar(profesor_id):
    eliminarProfesor(profesor_id)
    return redirect('/')

if __name__ == "__main__":
    app.run(debug=True)